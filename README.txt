Yorkshire Payments Online

Provides Yorkshire Payments integration with Ubercart.

------------------------------ SUMMARY ------------------------------
The Yorkshire Payments Online package allows your business to process card payments using either a Virtual Terminal, for phone or mail order transactions, or through an e-commerce shopping cart for website orders for just £16 per month. All transactions processed are either fully AVS compliant on the Virtual Terminal or 3d secure on the shopping cart for websites, ensuring that both businesses and customers alike can feel confident in the levels of security to safeguard information. Compatible with over 80 shopping carts and with full PCI Level 1 compliance our online offering provides everything businesses need accept card payments with confidence

---------------------------- Requirements / Dependencies ---------------------------
ubercart: Need to enable two module of Ubercart
1. uc_payment 
2. uc_credit 
enable both module...

---------------------------- INSTALLATION ---------------------------

* Install as usual, see http://drupal.org/node/895232 for further information.
* Place the entire "yorkshire" directory into your Drupal sites modules directory (eg sites/all/modules).
* Enable this module by navigating to: Administration > Modules.

------------------------------ Documentation and CONTACT ------------------------------

http://yorkshirepayments.com
Yorkshire Payments is a trading division of Direct Card Solutions Limited. 
